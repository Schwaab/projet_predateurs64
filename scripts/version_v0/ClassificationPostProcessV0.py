#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Auteur: Lucas Schwaab

Script qui prépare la carte d'occupation du sol 
classif_in: classification d'occupation du sol
hedge_topo: masque raster binaire pour les haies et bosquets (ex: BDtopo)
urban_mask: masque raster binaire pour (ex: Corine Land Cover)
DEM: MNT
tmp_dir: chemin de sortie pour les fichiers temporaires
output_dir: chemin de sortie pour les résultats

# --------------------------------------------------------------------------- # 
Author: Lucas Schwaab 

Script that applies post-processing steps. From iota2 classification to post-processed classification (i.e. ready to be used for vulnerability map)
classif_in: preporcessed input image
hedge_topo: preporcessed binary raster mask of hedges (eg. BDtopo)
urban_mask: preporcessed binary raster mask of urban areas (eg Corine Land Cover)
DEM: DEM
tmp_dir: temporary output path
output_dir: output path
"""

def AddHedgesAndMergeCropsOTBV0(image_in, hedge_in, tmp_dir):

    """
    Add hedges from external source (eg. BDtopo) using OTB and merges crops at the same time
    image_in: input classification image. Pixel values must match the ones given in the next parameters.
    hedge_in: binary raster with hedges. Values must be 1 for hedges and 0 otherwise. 
    tmp_dir: output path for temporary files
    """
    out_hedged=os.path.join(tmp_dir, "tmp_classif_add_BDtopo_OTB.tif")
    condition = "im2b1 == 1 and im1b1 > 0 ? 12 : im1b1 == 4 or im1b1 == 5 ? 5 : im1b1 == 2 or im1b1 == 3 or im1b1 == 6 ? 2 : im1b1"
    raw_extract = oab.CreateBandMathApplication({"il":[image_in, hedge_in], "exp":condition, "pixType":"uint8", "out":out_hedged})
    raw_extract.ExecuteAndWriteOutput()
    return out_hedged 
# --------------------------------------------------------------------------- # 
def ClassifyForestTopographyV0(classification_input, DEM_input, exposition, output_dir, lower_limit, upper_limit):
    """
    Divides the two initial forest classes into five forest classes using altitude and exposition
    classif_in: input classification image.
    DEM: DEM image 
    exposition: binary image of slope exposition. Values must be North: 1, Other: 0.
    output_dir: output path
    lower_limit: altitude limit seperating valley from mountain
    upper_limit: altitude limit seperating mountain from high moutain
    """
    out_forest_species=os.path.join(output_dir, "tmp_forest_species.tif")
    condition = "im1b1 == 12 ? im2b1 <" + str(lower_limit) +"? 12 : 13 : im1b1 == 13 ? im2b1 >"+ str(upper_limit) +"? 22 : im2b1 <"+ str(upper_limit) +"and im2b1 >" + str(lower_limit) + "? im3b1 == 1 ? 21 : 13 : im2b1 <"+ str(lower_limit) +"? 13 : im1b1 : im1b1"
    forest_topo_extract = oab.CreateBandMathApplication({"il":[classification_input, DEM_input, exposition], "exp":condition, "pixType":"uint8", "out":out_forest_species})
    forest_topo_extract.ExecuteAndWriteOutput()
    
    return out_forest_species

# --------------------------------------------------------------------------- # 
def OpeningDilateV0(in_image, tmp_dir, xr_open, yr_open, xr_dilate, yr_dilate):
    """
    Opening followed by an extra dilation on binary image 
    in_image: input classification image.
    tmp_dir: output path for temporary files
    xr_haies: xradius for small hedges
    yr_haies: yradius for small hedges
    xr_bois: xradius for medium size forests 
    yr_bois: yradius for medium size forests
    """
    #use binar images
    out_binaire=os.path.join(tmp_dir, "tmp_classif_binaire_forest.tif")
    if exists(out_binaire) == False :
        condition = "im1b1 == 12 or im1b1 == 13 ? 1 : 0"
        opendilate = oab.CreateBandMathApplication({"il":[in_image], "exp":condition, "pixType":"uint8", "out":out_binaire})
        opendilate.ExecuteAndWriteOutput()
    

    #small hedges
    app0 = otbApplication.Registry.CreateApplication("BinaryMorphologicalOperation")
    out_small=os.path.join(tmp_dir, "tmp_classif_small_hedges"+str(xr_open)+"_"+str(yr_open)+".tif")
    app0.SetParameterString("in", out_binaire)
    app0.SetParameterString("out", out_small)
    app0.SetParameterInt("channel", 1)
    app0.SetParameterString("structype", "ball")
    app0.SetParameterInt("foreval", 1)
    app0.SetParameterInt("xradius", xr_open)
    app0.SetParameterInt("yradius", yr_open)
    app0.SetParameterInt("ram", 4000)
    app0.SetParameterString("filter", "opening")
    app0.ExecuteAndWriteOutput()
    
    if xr_dilate > 0 and yr_dilate > 0: 
        app2 = otbApplication.Registry.CreateApplication("BinaryMorphologicalOperation")
        app2.SetParameterString("in", out_small)
        out_dilate=os.path.join(tmp_dir, "tmp_opening3_dilated.tif")
        app2.SetParameterString("out", out_dilate)
        app2.SetParameterInt("channel", 1)
        app2.SetParameterString("structype", "ball")
        app2.SetParameterInt("foreval", 1)
        app2.SetParameterInt("xradius", xr_dilate)
        app2.SetParameterInt("yradius", yr_dilate)
        app2.SetParameterInt("ram", 4000)
        app2.SetParameterString("filter", "dilate")
        app2.ExecuteAndWriteOutput()
        
        return (out_binaire, out_dilate)
    
    else: 
        return out_small
    

# --------------------------------------------------------------------------- # 
def AreaFilterRasterToPolygonV0(classif_in, tmp_dir, SQL_area):
    """
    Polygonize to filter polygons by area and re-rasterize
    classif_in: input image as a binary raster of forests without hedges 
    tmp_dir: output path for temporary files
    SQL_area: SQL statement for 
    """
    polygonized=os.path.join(tmp_dir, "tmp_hedges_extracted_otb.shp")
    print("polygonize")
    GdalPolygonize.GdalPolygonizeSubProcess(classif_in, polygonized)
    polygonized_forest=os.path.join(tmp_dir, "tmp_hedges_extracted_otb_forest_only.shp")
    SQL="DN in ('1')"
    exp.ExportByAttribute(polygonized, SQL, polygonized_forest)
    AddAreaField.AddAreaField(polygonized_forest)
    out_shp=os.path.join(tmp_dir, "tmp_hedges_extracted_otb_forest_filtered.shp")
    exp.ExportByAttribute(polygonized_forest, SQL_area , out_shp)
    out_raster_filtered=os.path.join(tmp_dir, "tmp_hedges_extracted_otb_area_filtered.tif")
    print("rasterize")
    Rasterize.rasterize(classif_in, out_shp, "DN", out_raster_filtered)
    return out_raster_filtered
    

# --------------------------------------------------------------------------- # 
def FilterUrbanHedgesV0(classif_in, urban_mask, output_dir):
    out_filter=os.path.join(output_dir, "tmp_hedges_extracted_urban_filter_otb.tif")
    condition = "im1b1 == 27 or im1b1 == 8 ? im2b1 == 1 ? 1 : im1b1 : im1b1"
    urbanhedges = oab.CreateBandMathApplication({"il":[classif_in, urban_mask], "exp":condition, "pixType":"uint8", "out":out_filter, "ram":"3000"})
    urbanhedges.ExecuteAndWriteOutput()
    
    return out_filter


# --------------------------------------------------------------------------- # 
def HedgeAndForestExtractionV0(image_input, hedge_input, DEM_input, tmp_dir, output_dir):
    
    """
    Main step:
    Erodes and dilates (opening) to eliminate forest elements smaller than ball shaped element. 
    Then uses Bandmath to classify eliminated elements as Hedges and filter a second time by area to make the result more homogeneous. 
    Classify different type of forests and hedges using topographic rules (altitude and aspect)
    image_in: input image 
    hedge_in: input hedges as binary raster mask of hedges (eg. BDtopo)
    DEM_input: DEM
    tmp_dir: output path for temporary files
    output_dir: output path for final result
    """
    
    #Add hedges from external source
    print("HedgeExtractionOpening")
    hedges_topo= AddHedgesAndMergeCropsOTBV0(image_input, hedge_input, tmp_dir)
    #Classify forest dominant species with topography
    
    #aspect subprocess = exposition
    out_aspect= os.path.join(tmp_dir, "tmp_exposition.tif")
    aspect = ["gdaldem", "aspect", DEM_input, out_aspect, "-of", "GTiff"]
    subprocess.call(aspect) 
    #aspect = north binary image    
    out_north=os.path.join(output_dir, "tmp_exposition_nord.tif")
    condition = "im1b1 < 90 or im1b1 > 270 ? 1 : 0"
    exposition_binary = oab.CreateBandMathApplication({"il":out_aspect, "exp":condition, "pixType":"uint8", "out":out_north})
    exposition_binary.ExecuteAndWriteOutput()
    
    
    in_hedges_small = OpeningDilateV0(hedges_topo, tmp_dir, 1, 1, 0, 0)
    out_hedges_small=os.path.join(tmp_dir, "tmp_hedges_small.tif")
    condition = "im1b1 == 12 or im1b1 == 13 ? im2b1 == 1 ?  im1b1 : 27 : im1b1"
    raw_extract = oab.CreateBandMathApplication({"il":[hedges_topo, in_hedges_small], "exp":condition, "pixType":"uint8", "out":out_hedges_small})
    raw_extract.ExecuteAndWriteOutput()
    
    print("Classify Forests using topography")
    hedges_and_species= ClassifyForestTopographyV0(out_hedges_small, DEM_input, out_north, tmp_dir, 1000, 1600)
    #Opening to extract hedges/forests
    print("Difference hedges/forests")
    (binaire_forest, ouverture_dilate) = OpeningDilateV0(out_hedges_small, tmp_dir, 3, 3, 3, 3)

    #Recover forests/non-forest from the opening 
    #If pixel = forest before and after the opening the "forest"
    out_filter=os.path.join(tmp_dir, "tmp_binaire_filtered.tif")
    condition = "im1b1 == 1 ? im2b1 == 1 ? 1 : 0 : 0"
    raw_extract2 = oab.CreateBandMathApplication({"il":[binaire_forest, ouverture_dilate], "exp":condition, "pixType":"uint8", "out":out_filter})
    raw_extract2.ExecuteAndWriteOutput()
    
    #Polygonize and filter by area 
    #After the opening some forest elements may have been seperated from each other and now form small clusters
    #So only keep polygons that are bigger than 5000 m2 
    print("Polygonize and filter by area")
    ouverture_dilate_area_filtered=AreaFilterRasterToPolygonV0(out_filter, tmp_dir, "area > 5000")
    
    
    #Add different values for each species/size combination
    out_hedges_all=os.path.join(tmp_dir, "tmp_hedges_extracted_otb.tif")
    condition = "im2b1 == 12 ? im1b1 == 1 ? 12 : 14 : im2b1 == 13 ? im1b1 == 1 ? 13 : 23 : im2b1 == 21 ? im1b1 == 1 ? 21 : 25 : im2b1 == 22 ? im1b1 == 1 ? 22 : 26 : im2b1"
    raw_extract3 = oab.CreateBandMathApplication({"il":[ouverture_dilate_area_filtered, hedges_and_species], "exp":condition, "pixType":"uint8", "out":out_hedges_all})
    raw_extract3.ExecuteAndWriteOutput()
      
    
    return out_hedges_all

# --------------------------------------------------------------------------- # 
def ClassifyMineralsV0(classif_in, in_DEM, in_landforms, slope_limit, tmp_dir, output_dir, output_name):
    """
    classif_in: input classification image 
    in_DEM: input DEM
    in_landforms: input landforms previously computed
    slope_limit: slope limit in ° for cliffs/edges
    tmp_dir: output path for temporary files
    output_dir: output path for final result
    https://gdal.org/programs/gdaldem.html
    """
    #slope subprocess
    out_slope= os.path.join(tmp_dir, "tmp_pente.tif")
    slope = ["gdaldem", "slope", in_DEM, out_slope, "-of", "GTiff"]
    subprocess.call(slope) 
    
    #cliffs = rocky mountain top + big slope
    out_cliffs=os.path.join(tmp_dir, "tmp_cliffs.tif")
    condition = "im1b1 >"+str(slope_limit)+" or im2b1 == 10 ? 1 : 0" #10 is the value for mountain tops
    min_extract = oab.CreateBandMathApplication({"il":[out_slope, in_landforms], "exp":condition, "pixType":"uint8", "out":out_cliffs})
    min_extract.ExecuteAndWriteOutput()
    #TODO: in memory 
    
    #include in the classification
    out_minerals=os.path.join(output_dir, output_name)
    condition = "im1b1 == 17 ? im2b1 == 1 ? 17 : 19 : im1b1"
    min_extract2 = oab.CreateBandMathApplication({"il":[classif_in, out_cliffs], "exp":condition, "pixType":"uint8", "out":out_minerals})
    min_extract2.ExecuteAndWriteOutput()
    
    return out_minerals

    
#%% Main _algo
    

 
import otbApplication     
from iota2.Common import OtbAppBank as oab
import GdalPolygonize
import Rasterize
import AddAreaField
import ExportByAttribute as exp
import ComputeTPI
import os, sys
from os.path import exists
import subprocess



def ClassificationPostProcessV0(classif_in, hedge_topo, urban_mask, DEM, tmp_dir, output_dir):
    """
    Main algorithm: from iota2 classification to post-processed classification ready to be used for vulnerability map
    classif_in: preporcessed input image
    hedge_topo: preporcessed binary raster mask of hedges (eg. BDtopo)
    urban_mask: preporcessed binary raster mask of urabn areas (eg Corine Land Cover)
    DEM: DEM
    """
    print("HedgeAndForestExtraction")
    output_hedged= HedgeAndForestExtractionV0(classif_in, hedge_topo, DEM, tmp_dir, output_dir)
    print("FilterUrbanHedges")
    output_filtered= FilterUrbanHedgesV0(output_hedged, urban_mask, tmp_dir)
    output_hedged = None
    print("computeLandforms")
    landforms= ComputeTPI.computeLandforms(DEM, 3, 30, os.path.join(output_dir, "landforms_64_3_30.tif"), tmp_dir)
    print("ClassifyMinerals")
    (input_name, ext)= os.path.splitext(os.path.split(classif_in)[1])
    output_name = str(input_name)+"_post_traitements.tif"
    output= ClassifyMineralsV0(output_filtered, DEM, landforms, "45", tmp_dir, output_dir, output_name)
    output_filtered = None
    return (output, landforms)


if __name__=='__main__':
    usage='usage: ClassificationPostProcessV0 <classif_in> <hedge_topo> <urban_mask> <DEM> <tmp_dir> <output_dir>'
    if len(sys.argv) == 7:
        if ClassificationPostProcessV0(sys.argv[1],sys.argv[2],sys.argv[3],sys.argv[4],sys.argv[5],sys.argv[6]):
            print ('Post processing')
            sys.exit(0)
        else:
            print ('Failed!')
            sys.exit(1)
    else:
        print (usage)
        sys.exit(1)
