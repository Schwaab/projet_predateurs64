"""
Auteur: Thierion Vincent

A partir de https://landscapearchaeology.org/2019/tpi/ de Zoran Čučković

Comme application de la méthode détaillée ici: http://www.jennessent.com/downloads/TPI-poster-TNC_18x22.pdf

"""
import os
import gdal
import numpy as np
import numpy.ma as ma
from iota2.Common import OtbAppBank as oab


def view (offset_y, offset_x, shape, step=1):
    """
    Function returning two matching numpy views for moving window routines.
    - 'offset_y' and 'offset_x' refer to the shift in relation to the analysed (central) cell 
    - 'shape' are 2 dimensions of the data matrix (not of the window!)
    - 'view_in' is the shifted view and 'view_out' is the position of central cells
    (see on LandscapeArchaeology.org/2018/numpy-loops/)
    """
    size_y, size_x = shape
    x, y = abs(offset_x), abs(offset_y)
    
    x_in = slice(x , size_x, step) 
    x_out = slice(0, size_x - x, step)

    y_in = slice(y, size_y, step)
    y_out = slice(0, size_y - y, step)
 
    # the swapping trick    
    if offset_x < 0: x_in, x_out = x_out, x_in                                 
    if offset_y < 0: y_in, y_out = y_out, y_in
 
    # return window view (in) and main view (out)
    return np.s_[y_in, x_in], np.s_[y_out, x_out]


def computeTPI(elevation_model, r, output_model):

    win = np.ones((2* r +1, 2* r +1))
    r_y, r_x  = win.shape[0]//2, win.shape[1]//2
    win[r_y, r_x] = 0

    dem = gdal.Open(elevation_model)
    srcband = dem.GetRasterBand(1)
    ndv = srcband.GetNoDataValue()
    demdata = dem.ReadAsArray()
    #demdata_masked = ma.masked_where(mx_z == ndv, mx_z)
    #meandem = np.mean(demdata_masked)
    
    #mx_z[mx_z==ndv] = np.mean(meandem)
    #mx_z[mx_z==ndv] = 0
    mx_z = ma.masked_where(demdata == ndv, demdata)
    # _, xres, _, _, _, yres  = dem.GetGeoTransform()
    # pixelarea = round(abs(xres) * abs(yres))
    
    #matrices for temporary data
    mx_temp = np.zeros(mx_z.shape)
    mx_count = np.zeros(mx_z.shape)

    # loop through window and accumulate values
    for (y,x), weight in np.ndenumerate(win):

        if weight == 0 : continue  #skip zero values !
        # determine views to extract data 
        view_in, view_out = view(y - r_y, x - r_x, mx_z.shape)
        # using window weights (eg. for a Gaussian function)
        mx_temp[view_out] += mx_z[view_in]  * weight
        
       # track the number of neighbours 
       # (this is used for weighted mean : Σ weights*val / Σ weights)
        mx_count[view_out] += weight

    # this is TPI (spot height – average neighbourhood height)
    out = mx_z - mx_temp / mx_count
    outnorm = normalizeTPI(out)
    
    # writing output 
    driver = gdal.GetDriverByName('GTiff')
    ds = driver.Create(output_model, mx_z.shape[1], mx_z.shape[0], 1, gdal.GDT_Float32)
    ds.SetProjection(dem.GetProjection())
    ds.SetGeoTransform(dem.GetGeoTransform())
    ds.GetRasterBand(1).WriteArray(outnorm)
    ds = None

# https://stackoverflow.com/questions/11686720/is-there-a-numpy-builtin-to-reject-outliers-from-a-list
def reject_outliers(data, m = 2.):
    d = np.abs(data - np.ma.median(data))
    mdev = np.ma.median(d)
    s = d/mdev if mdev else 0.
    return data[s<m]
    
def normalizeTPI(tpi):
    #mask_tpi = np.ma.masked_where(np.ma.getmask(demmask), tpi)
    # print(demmask.mask.shape)
    # mask_tpi = tpi[~demmask.mask].copy()
    tpi_mask = reject_outliers(tpi)
    mean = np.mean(tpi_mask)
    std = np.std(tpi_mask)
    tpi_stdi = ((((tpi - mean) / std) * 100) + 0.5).astype(int)
    
    return tpi_stdi

def computeLandforms(elevation_model, tpilowscale, tpihighscale, output, tmppath):
    # tpilowscale / tpihighscale => en nombre de pixels

    output = os.path.splitext(output)[0] + "_%s_%s.tif"%(tpilowscale, tpihighscale) 
    outlowscale = os.path.join(tmppath, "scale%s.tif"%(tpilowscale)) 
    outhighscale = os.path.join(tmppath, "scale%s.tif"%(tpihighscale))
    slopedeg = os.path.join(tmppath, "slope.tif")
    computeTPI(elevation_model, tpilowscale, outlowscale)
    print("Low scale TPI computed in %s"%(outlowscale))
    computeTPI(elevation_model, tpihighscale, outhighscale)
    print("High scale TPI computed in %s"%(outhighscale))
    os.system("gdaldem slope %s %s"%(elevation_model, slopedeg))
    print("Slope computed in %s"%(slopedeg))
    
    condition = "((im1b1>-100) and (im1b1<100) and (im2b1>-100) and (im2b1<100) and (im3b1<=5))?5:((im1b1>-100) and (im1b1<100) and (im2b1>-100) and (im2b1<100) and (im3b1>=6))?6:((im1b1>-100) and (im1b1<100) and (im2b1>=100))?7:((im1b1>-100) and (im1b1<100) and (im2b1<=-100))?4:((im1b1<=-100) and (im2b1>-100) and (im2b1<100))?2:(im1b1>=100) and (im2b1>-100) and (im2b1<100)?9:(im1b1<=-100) and (im2b1>=100)?3:((im1b1<=-100) and (im2b1<=-100))?1:((im1b1>=100) and (im2b1>=100))?10:((im1b1>=100) and (im2b1<=-100))?8:11"

    landforms = oab.CreateBandMathApplication({"il":[outlowscale, outhighscale, slopedeg], "exp":condition, "pixType":"uint8", "out":output})
    landforms.ExecuteAndWriteOutput()
    print("Landforms computed in %s"%(output))
    
    return output
