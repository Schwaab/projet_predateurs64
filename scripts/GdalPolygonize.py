#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""

https://gdal.org/api/gdal_alg.html


GDALPolygonize  (GDALRasterBandH    hSrcBand,
                 GDALRasterBandH    hMaskBand,
                 OGRLayerH          hOutLayer,
                 int                iPixValField,
                 char **            papszOptions,
                 GDALProgressFunc   pfnProgress,
                 void *             pProgressArg 
                )

"""

import gdal
import ogr 
import os


def GdalPolygonize(classif_original, outfile):
    dataset = gdal.Open(classif_original)
    dst_band= dataset.GetRasterBand(1)
    srsObj = dataset.GetSpatialRef()
    print(srsObj)

    
    outDriver = ogr.GetDriverByName("ESRI Shapefile")
    
    if os.path.exists(outfile):
        outDriver.DeleteDataSource(outfile)
    outDataSource = outDriver.CreateDataSource(outfile)

    #Creates the spatial reference of the output layer
    out_layer = outDataSource.CreateLayer(outfile, srsObj, geom_type = ogr.wkbPolygon)
    gdal.Polygonize(dst_band, dst_band, out_layer, -1)
        
        
def GdalPolygonizeSubProcess(in_file, out_file):
    import subprocess
    cmdline = ['gdal_polygonize.py', in_file, out_file]
    subprocess.call(cmdline)
