#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import gdal
import ogr    

def addField(filein,nameField,valueField):
    source = ogr.Open(filein, 1)
    layer = source.GetLayer()
    layer_defn = layer.GetLayerDefn()
    field_names = [layer_defn.GetFieldDefn(i).GetName() for i in range(layer_defn.GetFieldCount())]
    print(field_names)
    try :
        int(valueField)
        new_field1 = ogr.FieldDefn(nameField, ogr.OFTInteger)
    except :
        new_field1 = ogr.FieldDefn(nameField, ogr.OFTString)
    
    layer.CreateField(new_field1)
    for feat in layer:
        layer.SetFeature(feat)
        feat.SetField(nameField, valueField)
        layer.SetFeature(feat)
    return 0

def rasterize(data, vectorSrc, field, outFile):
    dataSrc = gdal.Open(data)
    shp = ogr.Open(vectorSrc)

    lyr = shp.GetLayer()


    driver = gdal.GetDriverByName('GTiff')
    dst_ds = driver.Create(
        outFile,
        dataSrc.RasterXSize,
        dataSrc.RasterYSize,
        1,
        gdal.GDT_UInt16)
    dst_ds.SetGeoTransform(dataSrc.GetGeoTransform())
    dst_ds.SetProjection(dataSrc.GetProjection())
    if field is None:
        gdal.RasterizeLayer(dst_ds, [1], lyr, None)
        print("Field is None")
    else:
        OPTIONS = ['ATTRIBUTE=' + str(field)]
        gdal.RasterizeLayer(dst_ds, [1], lyr, None, options=OPTIONS)

    data, dst_ds, shp, lyr = None, None, None, None
    return outFile 


