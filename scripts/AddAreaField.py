#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Adds area field
"""
from osgeo import ogr
import sys


def AddAreaField(filein):
    source = ogr.Open(filein, 1)
    layer = source.GetLayer()
    #
    layer.CreateField(ogr.FieldDefn('area', ogr.OFTInteger))
    for feat in layer:
        layer.SetFeature(feat)
        feat.SetField('area', feat.geometry().Area())
        layer.SetFeature(feat)
        
    return 0

if __name__=='__main__':
    usage='usage: <infile> <nameField> <value>'
    if len(sys.argv) == 4:
        AddAreaField(sys.argv[1],sys.argv[2],sys.argv[3])# == 0:
        print('Add of field succeeded!')
        #sys.exit(0)
        #else : print "pb"
    else:
        print(usage)
        sys.exit(1)
        
        
        
        
        
        
