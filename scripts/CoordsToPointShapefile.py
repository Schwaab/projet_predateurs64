#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
@author: Lucas Schwaab

Functions to turns GPS points in point shapefile
(used when collecting ground truth in the form of GPS points)
"""

from osgeo import ogr
import csv



def CoordsTopointsShapefile(coords, out_shp, template_shape):

    driver = ogr.GetDriverByName('Esri Shapefile')
    
    # Get SpatialRef from template
    ds_template = driver.Open(template_shape, 0)
    template_layer = ds_template.GetLayer()
    spatialRef = template_layer.GetSpatialRef()
    
    #    
    ds = driver.CreateDataSource(out_shp)
    layer = ds.CreateLayer(out_shp, spatialRef, ogr.wkbPoint)
    
    # 
    layer.CreateField(ogr.FieldDefn('FID', ogr.OFTInteger))
    defn = layer.GetLayerDefn()

    # Create a new feature 
    feat = ogr.Feature(defn)
    feat.SetField('FID', 1)

    # Make a geometry
    i=1
    for coord in coords:
        point = ogr.Geometry(ogr.wkbPoint)
        point.AddPoint(coord[0], coord[1])
        feat.SetField('FID', i)
        i+=1
        feat.SetGeometry(point)
        layer.CreateFeature(feat)

    # Save and close 
    ds = layer = feat = point = None
    
    return layer


def CsvCoordsTopointsShapefile(csv_in, out_shp, template_shape):

    driver = ogr.GetDriverByName('Esri Shapefile')
    
    # Get SpatialRef from template
    ds_template = driver.Open(template_shape, 0)
    template_layer = ds_template.GetLayer()
    spatialRef = template_layer.GetSpatialRef()
    
    #   
    ds = driver.CreateDataSource(out_shp)
    layer = ds.CreateLayer(out_shp, spatialRef, ogr.wkbPoint)
    
    layer.CreateField(ogr.FieldDefn('FID', ogr.OFTInteger))
    layer.CreateField(ogr.FieldDefn('Class', ogr.OFTString))
    defn = layer.GetLayerDefn()

    # Create a new feature
    feat = ogr.Feature(defn)

    #
    with open(csv_in, newline='') as f:
        reader = csv.reader(f)
        coords = [tuple(row) for row in reader]

    # Make geometry
    i=1
    for coord in coords:
        point = ogr.Geometry(ogr.wkbPoint)
        point.AddPoint(float(coord[0]), float(coord[1]))
        feat.SetField('FID', i)
        feat.SetField('Class', coord[2])
        i+=1
        feat.SetGeometry(point)
        layer.CreateFeature(feat)

    # Save and close 
    ds = layer = feat = point = None
    
    return layer

#out_shp="/home/schwaabl/donnees/releves_terrain/Releve_pts_geoportail.shp"
#template="/home/schwaabl/donnees/releves_terrain/Points_HB_2017_2154.shp"
#csv_in="/home/schwaabl/donnees/releves_terrain/Releve_pts_geoportail.csv"
#CsvCoordsTopointsShapefile(csv_in, out_shp, template)
