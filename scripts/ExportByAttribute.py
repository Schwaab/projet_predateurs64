#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
@author: Lucas Schwaab

Exports shapefiles using SQL statements
"""

import ogr
import os

def ExportByAttribute(inShapefile, SQL, outShapefile):
    # Get the input Layer
    inDriver = ogr.GetDriverByName("ESRI Shapefile")
    inDataSource = inDriver.Open(inShapefile, 0)
    inLayer = inDataSource.GetLayer()
    inLayer.SetAttributeFilter(SQL)
    inLayerDefn = inLayer.GetLayerDefn()
    field_name_target = []
    for i in range(inLayerDefn.GetFieldCount()):
        field =  inLayerDefn.GetFieldDefn(i).GetName()
        field_name_target.append(field)


    # Create the output LayerS
    outDriver = ogr.GetDriverByName("ESRI Shapefile")

    # Remove output shapefile if it already exists
    if os.path.exists(outShapefile):
        outDriver.DeleteDataSource(outShapefile)

    # Create the output shapefile
    outDataSource = inDriver.CreateDataSource(outShapefile)
    #Get the spatial reference of the input layer
    srsObj = inLayer.GetSpatialRef()
    #Creates the spatial reference of the output layer
    outLayer = outDataSource.CreateLayer(outShapefile, srsObj, geom_type=inLayer.GetGeomType() )
    # Add input Layer Fields to the output Layer if it is the one we want
    #inLayerDefn = inLayer.GetLayerDefn()
    for i in range(0, inLayerDefn.GetFieldCount()):
        fieldDefn = inLayerDefn.GetFieldDefn(i)
        fieldName = fieldDefn.GetName()
        if fieldName not in field_name_target:
            continue
        outLayer.CreateField(fieldDefn)

    # Get the output Layer's Feature Definition
    outLayerDefn = outLayer.GetLayerDefn()
    count = 0
    # Add features to the ouput Layer
    for inFeature in inLayer:
        count += 1
        #left = inLayer.getFeatureCount() - count
        if count % 100 == 0: 
            print("100 Features")
            #print(str(left) + "Features left")
        
        # Create output Feature
        outFeature = ogr.Feature(outLayerDefn)

        # Add field values from input Layer
        for i in range(0, outLayerDefn.GetFieldCount()):
            fieldDefn = outLayerDefn.GetFieldDefn(i)
            fieldName = fieldDefn.GetName()
            if fieldName not in field_name_target:
                continue

            outFeature.SetField(outLayerDefn.GetFieldDefn(i).GetNameRef(),
                inFeature.GetField(i))

        # Set geometry as centroid
        geom = inFeature.GetGeometryRef()
        outFeature.SetGeometry(geom.Clone())
        # Add new feature to output Layer
        outLayer.CreateFeature(outFeature)
        outFeature = None

    # Save and close DataSources
    inDataSource = None
    outDataSource = None
    
    return outShapefile
    
