# Projet_predateurs64

Projet de cartographie de la vulnérabilité paysagère locale des troupeaux au pâturage face aux attaques de prédateurs dans les Pyrénées-Atlantiques. 



# Déscriptif du contenu 

# configfiles

Exemples de fichiers de configuration iota2 pour la classification d'occupation du sol 

- **configfile_v3_autocontext_ref18_2017-2018_TR20_3z.cfg**: classification autocontext avec la donnée de référence de 2018, sur la période 2017-2018 (résolution temporelle 20j pour le Gapfilling) avec stratification 3 zones

- **configfile_v3_ref18_2017-2018_TR20_4z.cfg**: classification avec la donnée de référence de 2018, sur la période 2017-2018 (résolution temporelle 20j pour le Gapfilling) avec stratification 4 zones

- **configfile_v3_ref18_2018-2019_TR20_3z.cfg**: classification avec la donnée de référence de 2018, sur la période 2017-2018 (résolution temporelle 20j pour le Gapfilling) avec stratification 3 zones

# distances_csv

- **distances.csv**: fichier csv de paramétrage des distances de vulnérabilité associées aux classes sources

- **target_classes.csv**: fichier csv de paramétrage des classes cibles et des vulnérabilités intrinsèques associées 

# nomenclature

- **colorFile.txt**: fichier de couleurs pour la classification d'occupation du sol

- **nomenclature.txt**: nomenclature de la classification d'occupation du sol

# scripts

- **AddAreaField**: ajout d'un champ pour la surface dans un shapefile. 

- **ClassificationPostProcess**: étape de post-traitement de la classification pour la préparer à l'étape de calcul de la vulnérabilité paysagère locale. 

- **ClassificationToVulnerability**: algorithme principal qui regroupe toutes les étapes permettant de passer de la classification à la carte de vulnérabilité. Inclut une étape de prétraitement qui permet de partir de données brutes. 

- **ComputeTPI**: calcul des formes topographiques à partir du TPI.

- **CoordsToPointShapefile**: création d'un shapefile ponctuel à partir de coordonnées GPS utilisé pour la récolte de données "terrain" pour la photo-interprétation.

- **ExportByAttribute**: exporte uniquement les entités d'un shapefile qui remplissent la condition SQL donnée.

- **GdalPolygonize**: conversion de raster à vecteur.  

- **LandscapeVulnerability**: étape de caclul de la carte de vulnérabilité à partir de la classification déjà préparée en post-traitement. 

- **OtbAppBank**: script iota2 qui facilite l'utilisation de l'OTB dans python (http://osr-cesbio.ups-tlse.fr/echangeswww/TheiaOSO/iota2_documentation/develop/_autosummary/iota2.Common.OtbAppBank.html)

- **PredatorVulnerabilityBinary**: tout premier prototype de code shell pour la carte de vulnérabilité.

- **Rasterize**: conversion de vecteur à raster

- **version_v0**: dossier comprenant les versions des scripts pour la 


# Production de la carte de vulnérabilité

La production de la carte de vulnérabilité à partir de la carte d'occupation du sol se fait en deux étapes:


- 1- Post-traitements à l'aide du script **ClassificationPostProcess**

**usage**: 

**ClassificationPostProcess** -classif_in -hedge_topo -urban_mask -DEM -tmp_dir -output_dir  

**classif_in**: carte d'occupation du sol  
**hedge_topo**: donnée de référence (masque) pour les haies et bosquets (format raster, si format shp alors activer l'étape de prétraitement)   
**urban_mask**: donnée de référence (masque) pour les zones urbaines (format raster, si format shp alors activer l'étape de prétraitement)  
**DEM**: MNT  
**distances_csv**: fichier csv qui contient la liste des classes "source" et les distances et niveaux de vulnérabilité associés (voir aide)  
**target_classes_csv**: fichier csv qui contient la liste des classes "cible" avec les niveaux de vulnérabilité associés (voir aide)  
**temp_dir**: chemin de sortie pour les fichiers temporaires  
**output_dir**: chemin de sortie pour les résultats  



- 2- Calcul de la vulnérabilité à l'aide du script **LandscapeVulnerability**

**usage**: 

**LandscapeVulnerability** -image_in -DEM_in -landforms_in -distances_csv -target_classes_csv -tmp_path -output_dir -mode  

**image_in**: carte d'occupation du sol  
**DEM_in**: MNT  
**distances_csv**: fichier csv qui contient la liste des classes "source" et les distances et niveaux de vulnérabilité associés (voir aide)  
**target_classes**: fichier csv qui contient la liste des classes "cible" avec les niveaux de vulnérabilité associés (voir aide)  
**tmp_path**: chemin de sortie pour les fichiers temporaires  
**output_dir**: chemin pour la carte de vulnérabilité en sortie  
**mode**: "maximum" ou "sum" (somme) suivant le mode de calcul de la vulnérabilité choisi ("maximum" conserve la vulnérabilité maximale entre les différentes sources de vulnérabilité tandis que "sum" permet d'en faire s'additionner certaines)  


Les deux étapes sont regroupées dans le script **ClassificationToVulnerability**  

**usage1 (sans l'étape de prétraitement)**: 

**ClassificationToVulnerability** -image_in -hedge_mask -urban_mask -DEM_in -distances_csv -target_classes_csv -tmp_path -gdal_path -output_dir -mode -preporcess -delete_tmp  

**usage2 (avec l'étape de prétraitement)**: 

**ClassificationToVulnerability** -image_in -hedge_mask -urban_mask -DEM_in -distances_csv -target_classes_csv -tmp_path -gdal_path -output_dir -mode -preporcess -SQL_urban -SQL_hedges -template -delete_tmp  

**classif_in**: carte d'occupation du sol  
**hedge_mask**: donnée de référence (masque) pour les haies et bosquets (format raster, si format shp alors activer l'étape de prétraitement)  
**urban_mask**: donnée de référence (masque) pour les zones urbaines (format raster, si format shp alors activer l'étape de prétraitement)  
**DEM**: MNT  
**distances_csv**: fichier csv qui contient la liste des classes "source" et les distances et niveaux de vulnérabilité associés (voir aide)  
**target_classes_csv**: fichier csv qui contient la liste des classes "cible" avec les niveaux de vulnérabilité associés (voir aide)  
**tmp_path**: chemin de sortie pour les fichiers temporaires 
**gdal_path**: chemin de sortie pour les fichiers temporaires de l'étape gdal_proximity
**output_dir**: chemin de sortie pour les résultats  
**mode**: "maximum" ou "sum" (somme) suivant le mode de calcul de la vulnérabilité choisi ("maximum" conserve la vulnérabilité maximale entre les différentes sources de vulnérabilité tandis que "sum"  
**preprocess**: étape de pré-traitements pour les hedge_mask et urban_mask  
**SQL_urban**: expression SQL pour extraire les haies et bosquets du shapefile (si preprocess = "True")   
	exemple: si c'est la BDTOPO qui est fournie comme hedge_mask alors mettre "NATURE in ('Haie', 'Bois')"  
**SQL_hedges**: expression SQL pour extraire l'urbain du shapefile (si preprocess = "True")  
	exemple: si c'est CLC qui est fournie comme urban_mask alors mettre "code18 in ('111', '112', '121', '122', '123', '124', '131', '132', '133', '141', '142')"  
**template**: couche de référence pour l'étape de prétraitement
**delete_tmp**: supprimer les fichiers temporaires "True" ou "False"  


# Librairies 

L'ensemble des librairies nécessaire pour faire tourner ces scripts sont automatiquement installées lors de l'installation de Iota2.


# Nomenclature

La vulnérabilité pasyagère se calcul notamment en associant des niveaux de vulnérabilité aux différentes classes d'occupation du sol. Les outils mis à disposition ici, bien qu'ayant été développés dans un souci de généralisation, restent fortement liés à la carte d'occupation du sol des Pyrénées Atlantique crée spécifiquement dans le cadre du projet et à sa nomenclature. L'utilisation de cartes d'occupation du sol ayant une nomenclure différente peut donc nécessiter une modification du code, en particulier en cas de présence de classes supplémentaires (l'absence d'une ou plusieurs classes ne posant en revanche pas problème). 



# Fichiers csv


distances_csv: fichier csv qui contient la liste des classes "source" et les distances et niveaux de vulnérabilité associés 

distance (en pixels),code classe,niveau de vulnérabilité, groupe (pour le mode "sum")  
20,5,2,2  
20,8,2,2  
40,9,3,2  
40,10,2,2  
20,13,3,2  
20,14,3,2  
20,17,3,1  
20,16,2,1  
20,18,2,2  
40,21,1,2  
40,22,1,2  
20,25,1,1  
20,26,1,1  
10,27,1,1  



target_classes: fichier csv qui contient la liste des classes "cible" avec les niveaux de vulnérabilité associés  

code classe,niveau de vulnérabilité (intrinsèque),forêt/non forêt  
8,2,0  
7,0,0  
9,7,1  
10,6,1  
11,0,0  
12,0,0  
13,3,0  
14,3,0  
19,0,0  
17,7,1  
18,6,1  
21,4,1  
22,3,1  
25,1,1  
26,1,1  
27,1,0  
